# StrathE2E2examples
R package containing example results for StrathE2E2

## General Requirements

Install *StrathE2E2* first, binary packages for which are on the [Marine Resource Modelling website](https://marineresourcemodelling.gitlab.io/) in a CRAN compatible respository:
```
R
> install.packages("StrathE2E2", repos="https://marineresourcemodelling.gitlab.io/sran")
```

## Installing the Example Results

Within *StrathE2E*, load a standard model and invoke a function that requires example results. This will invoke an automatic download and install of the this data package:

```
> library(StrathE2E2)
> model <- e2e_read("North_Sea","1970-1999")
> e2e_plot_eco(model, selection="NUT_PHYT", ci.data=TRUE, use.example=TRUE)
Warning: the data package 'StrathE2E2examples' is required when using example results!
Do you want to install the required data package from the MarineResourceModelling repository ? (Yes/no/cancel) Yes
Installing package into ‘/home/cais04/Rlibs’
(as ‘lib’ is unspecified)
trying URL 'https://marineresourcemodelling.gitlab.io/sran/src/contrib/StrathE2E2examples_3.2.0.tar.gz'
Content type 'application/gzip' length 6122840 bytes (5.8 MB)
==================================================
downloaded 5.8 MB

* installing *source* package ‘StrathE2E2examples’ ...
** using staged installation
** R
** data
*** moving datasets to lazyload DB
** byte-compile and prepare package for lazy loading
** help
*** installing help indices
  converting help for package ‘StrathE2E2examples’
    finding HTML links ... done
    StrathE2E2examples-package              html  
** building package indices
** testing if installed package can be loaded from temporary location
** testing if installed package can be loaded from final location
** testing if installed package keeps a record of temporary installation path
* DONE (StrathE2E2examples)

The downloaded source packages are in
        ‘/tmp/RtmpbmlrWP/downloaded_packages’
Reading example results from StrathE2E2examples data package for the North_Sea 1970-1999 model
Plot Offshore zone upper layer and Suspended bact & detritus 
Plot Offshore zone upper layer and Ammonia 
Plot Offshore zone upper layer and Nitrate 
Plot Offshore zone upper layer and Phytoplankton 
Plot Offshore zone lower layer and Suspended bact & detritus 
Plot Offshore zone lower layer and Ammonia 
Plot Offshore zone lower layer and Nitrate 
Plot Offshore zone lower layer and Phytoplankton 
Plot Inshore zone and Suspended bact & detritus 
Plot Inshore zone and  Ammonia 
Plot Inshore zone and Nitrate 
Plot Inshore zone and Phytoplankton 
```

## Re-building the data

This is slightly complicated by the fact we need to add the freshly built data package to the website repository.

### Clone Repositories

If you haven't already done so, clone this repository AND the marineresourcemodelling.gitlab.io website repository:

```
  cd myrepos
  git clone git@gitlab.com:MarineResourceModelling/marineresourcemodelling.gitlab.io.git:
  Cloning into 'marineresourcemodelling.gitlab.io'...
  ...
  git clone git@gitlab.com:MarineResourceModelling/StrathE2E/strathe2e2examples.git
  Cloning into 'strathe2e2examples'...
  ...
```

### Rebuild this data package

Modify the data package as required (bumping version in DESCRIPTION or updating some of the example results).

Go to the *data-raw* directory rebuild the data object:

```
% cd strathe2e2examples/data-raw
% Rscript data_prep.R
```
This will rewrite the *example.results.rda* file within the *strathe2e2examples/data/* directory.

Move back up one level and refresh the documentation:

```
% cd ..
% Rscript -e 'devtools::document(".")'
```

Commit changes:
```
git add --all .
git commit -m "Updated version"
git push origin master
```

Now move up one more level and build the data package for SRAN:
```
cd ..
R CMD build strathe2e2examples
```

This will produce the R data package *StrathE2E2examples_3.2.0.tar.gz* for example.

### Install into the SRAN Repository on the website

Copy the newly built data package to the SRAN folder within the website repository:
```
cp StrathE2E2examples_3.2.0.tar.gz marineresourcemodelling.gitlab.io/public/sran/src/contrib
```

Rebuild the SRAN package index files (PACKAGES, PACKAGES.gz):
```
cd marineresourcemodelling.gitlab.io
Rscript -e 'tools::write_PACKAGES("public/sran/src/contrib/")'
```

and a final commit:
```
git add --all .
git commit -m "New data package version"
git push origin master
```

